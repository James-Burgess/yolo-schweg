import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    component: () => import('@/views/home/index')
  },
  {
    path: '/search/',
    component: () => import('@/views/products/list')
  },
  {
    path: '/detail/',
    component: () => import('@/views/products/detail')
  },
  {
    path: '/create/',
    component: () => import('@/views/products/create')
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
