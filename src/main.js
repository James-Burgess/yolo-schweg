import Vue from 'vue'

// import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import '@/assets/css/tailwind.css'

import App from './App'
// import store from './store'
import router from './router'
//
// import './mock' // simulation data

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  // store,
  render: h => h(App)
})
